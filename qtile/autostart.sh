#!/usr/bin/env bash 

nm-applet &
picom &
lxpolkit &
nitrogen --restore
