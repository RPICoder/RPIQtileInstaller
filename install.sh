#!/bin/bash

function error {
  echo -e "\\e[91m$1\\e[39m"
  exit 1
}


if [ ! -d "$HOME/RPIQtileInstaller" ];then
    git clone https://github.com/techcoder20/RPIQtileInstaller ~/RPIQtileInstaller || error "Failed To Clone Repo"
fi

sudo apt -y update || error "Failed To Update"
sudo apt -y install python3-pip libxcb-render0-dev libffi-dev libcairo2 libpangocairo-1.0-0 xserver-xorg libgdk-pixbuf2.0 xinit  x11-utils  pulseaudio git lxappearance xfce4-terminal nitrogen fonts-fantasque-sans thunar rofi lxpolkit || error "Failed To Install Dependencies"

pip3 install --user dbus-next xcffib || error "Failed To Install Pip Dependencies"
pip3 install --no-cache-dir cairocffi || error "Failed To Install Pip Dependencies"
python3 ~/.local/lib/python3.7/site-packages/cairocffi/ffi_build.py 
pip3 install qtile || error "Failed To Install Qtile"
sudo cp /home/pi/.local/bin/qtile /bin || error "Failed To Move Qtile Binary to /bin"

# Creating Xinit File
sudo mv /etc/X11/xinit/xinitrc /etc/X11/xinit/xinitrc.bak || error "Failed To Backup xinit"
echo '''
#!/bin/sh
exec qtile start ''' | sudo tee -a /etc/X11/xinit/xinitrc || error "Failed To Create Xinitrc"

# Installing Gtk Theme
wget https://github.com/EliverLara/Nordic/releases/download/2.0.0/Nordic-bluish-accent.tar.xz || error "Failed To Download Gtk Theme"
tar -xf Nordic-bluish-accent.tar.xz || error "Failed To Extract Gtk Theme"
sudo rm Nordic-bluish-accent.tar.xz || error "Failed To Remove Gtk Theme"
mkdir /home/pi/.themes 
mv Nordic-bluish-accent /home/pi/.themes/ || error "Failed To Install Gtk Theme"

# Installing Icon Theme
wget -qO- https://git.io/papirus-icon-theme-install | sh || error "Failed To Install Icon Theme"

# Installing Fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FantasqueSansMono.zip || error "Failed To Download Nerd Fonts"
unzip FantasqueSansMono.zip || error "Failed To Extract Nerd Fonts"
rm FantasqueSansMono.zip
mkdir ~/.fonts
mv *.ttf ~/.fonts

# Changing Themes
gsettings set org.gnome.desktop.interface gtk-theme "Nordic-bluish-accent"
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'

# Installing Qtile Configs
rm -r ~/.config/qtile
cp -r ~/RPIQtileInstaller/qtile ~/.config

# Installing Rofi Theme
cp -r ~/RPIQtileInstaller/rofi ~/.config

# Installing Wallpaper
mkdir ~/.local/share/wallpapers
cp ~/RPIQtileInstaller/Nord_Wallpaper.jpeg ~/.local/share/wallpapers
cp -r ~/RPIQtileInstaller/nitrogen ~/.config
